drop table book;
drop table author;
drop sequence Seq_author;
drop sequence Seq_book;


create sequence Seq_author ;
create sequence Seq_book ;

create table author (
id number(2) constraint author_pk primary key,
lastname varchar2(50),
firstname varchar2(50)
);


create table book (
  id number(2) constraint book_pk primary key,
  title varchar2(50),
  ISBN number(2), 
  id_author constraint author_ref references author(id)
) ;

insert into author(id,lastname,firstname) values (Seq_author.nextval,'Canet','Guillaume') ;
insert into author(id,lastname,firstname) values (Seq_author.nextval,'Debbouze','Jamel') ;
insert into author(id,lastname,firstname) values (Seq_author.nextval,'Tautou','Audrey') ;
insert into author(id,lastname,firstname) values (Seq_author.nextval,'Moreau','Yolande') ;



insert into book (id, title) values (Seq_book.nextval,'Asterix le gaulois') ;
insert into book (id, title) values (Seq_book.nextval,'Tintin au tibet') ;
insert into book (id, title) values (Seq_book.nextval,'La zizanie') ;
insert into book (id, title) values (Seq_book.nextval,'XIII - la nuit du 3 août') ;
insert into book (id, title) values (Seq_book.nextval,'Un goût de rouille et d''os') ;
insert into book (id, title) values (Seq_book.nextval,'Introduction aux bases de données') ;
insert into book (id, title) values (Seq_book.nextval,'Voyage au centre de la terre') ;
insert into book (id, title) values (Seq_book.nextval,'Le Papyrus de César');
insert into book (id, title) values (Seq_book.nextval,'Au bonheur des dames');
insert into book (id, title) values (Seq_book.nextval,'Germinal');
insert into book (id, title) values (Seq_book.nextval,'Le Horla');
insert into book (id, title) values (Seq_book.nextval,'Une vie');
insert into book (id, title) values (Seq_book.nextval,'Madame Bovary');
insert into book (id, title,id_author) values (Seq_book.nextval,'La cuisine pour les nuls',1);


select * 
from book;