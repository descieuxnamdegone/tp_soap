package biblio_client;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import biblio.IBiblitheque;
import biblio.ws.BibliothequeImp;

public class ClientBibliotheque {
	 public static void main (String[] args) {
		   
		   URL url = null;
		try {
			url = new URL("http://localhost:9998/ws/biblio?wsdl");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		};
		   QName qname   = new QName("http://ws.biblio/", "BibliothequeImp");
		   Service service = Service.create(url, qname);
		   IBiblitheque biblio = service.getPort(IBiblitheque.class);
		   System.out.println(biblio.lireTableTest());			   
		   
	   }

}
