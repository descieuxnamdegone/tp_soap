package biblio.ws;

import javax.jws.WebService;

import biblio.IBiblitheque;

import java.sql.*;
import java.io.*;
//import oracle.jdbc.pool.OracleDataSource; // pour OracleDataSource
import javax.sql.DataSource;


@WebService(endpointInterface = "biblio.IBiblitheque")

public class BibliothequeImp implements IBiblitheque{
	
	//private static OracleDataSource ods ; // variable de classe donc partagée par les instances
	private java.sql.Connection connect;
	private java.sql.Statement stmt;
	
	
	public BibliothequeImp(String login, String passwd) {
			
		try {
			this.connect = DriverManager.getConnection("jdbc:oracle:thin:@oracle.fil.univ-lille1.fr:1521:filora", login, passwd);
			
			this.stmt = this.connect.createStatement();
		} catch (SQLException e) {
			System.err.println("pb de creation du Statement : " + e.getErrorCode() + " " + e.getMessage());
		}
	}
	
	
	
	public void seDeconnecter() {
		try {
			this.connect.close();
		} catch (SQLException e) {
			System.err.println("erreur déconnexion " + e.getErrorCode() + " " + e.getMessage());
		}
	}
	
	public void AddBook(int id, String title) {
		try {
			this.stmt.executeUpdate(
					"insert into book(id, title) values(" + id + ",'" + title + "')");

		} catch (SQLException e) {
			System.err.println("pb insertion ligne dans book " + e.getErrorCode() + " " + e.getMessage());
		}
	}
	public void AddAuthor(int id, String lastname, String firstname) {
		try {
			this.stmt.executeUpdate(
			        "insert into author(id, lastname, firstname) values(" + id + ",'" + lastname + "','" + firstname + "')");

		} catch (SQLException e) {
			System.err.println("pb insertion ligne dans author " + e.getErrorCode() + " " + e.getMessage());
		}
	}
	
	
	public void lireTableTest() {
		ResultSet res ;
		String sqlReq = "select * from book";
		try {
			this.stmt = this.connect.createStatement();
			res = stmt.executeQuery(sqlReq);
			while (res.next()) {
				int id = res.getInt("ID");
				String ch =res.getString("TITLE");
				System.out.println("id :"+" "+ id+ " "+"title :  "  +ch);
			}
		}  catch  (SQLException e) {
			System.err.println("erreur déconnexion " + e.getErrorCode() + " " + e.getMessage() );
		}
		
	}
		
	public static void main(String args[]) {
		// lecture d'un nom d'utilisateur et d'un mot de passe
		//Console console = System.console();
		//String name = console.readLine("namdegone");
		//char[] pdt = console.readPassword("baleriana");
		//String passdata = new String(pdt);
		String log = "namdegone";
		String pass = "baleriana";

		// on teste les méthodes
		BibliothequeImp obj = new BibliothequeImp(log, pass); // connexion via le constructeur
		System.out.println("ok ouverture connexion obj");
		
		
		//obj.AddBook(24, "Germinal");
		//obj.AddAuthor(7, "Debbouze", "Jamel");
		obj.lireTableTest();

		
		
		// on se déconnecte
		obj.seDeconnecter();
		System.out.println("ok fermeture connexion obj");
		
	}
	
	
	
	

}
