package biblio;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

@WebService
@SOAPBinding(style = Style.RPC)
public interface IBiblitheque {
	
	@WebMethod
	
	public void AddBook(int id, String title) ;
	@WebMethod

	public void AddAuthor(int id, String lastname, String firstname) ;
	@WebMethod

	public void lireTableTest() ;


}
